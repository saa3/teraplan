package org.teraplan.demo.controllers;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.teraplan.demo.entities.*;
import org.teraplan.demo.repositories.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;

@RestController
public class DataController {

    private final FlightRepository flightRepository;
    private final EmployeeRepository employeeRepository;
    private final PairingRepository pairingRepository;
    private final LegRepository legRepository;
    private final AssignmentRepository assignmentRepository;

    private final JdbcTemplate jdbcTemplate;

    public DataController(FlightRepository flightRepository, EmployeeRepository employeeRepository, PairingRepository pairingRepository, LegRepository legRepository, AssignmentRepository assignmentRepository, JdbcTemplate jdbcTemplate) {
        this.flightRepository = flightRepository;
        this.employeeRepository = employeeRepository;
        this.pairingRepository = pairingRepository;
        this.legRepository = legRepository;
        this.assignmentRepository = assignmentRepository;
        this.jdbcTemplate = jdbcTemplate;
    }

    @RequestMapping("getObjectInfo")
    public Object[] getObjectInfo(@RequestParam("objectName") String objectName) {

        ArrayList<String> fieldInfo = new ArrayList<>();
        Class mClassObject;

        switch (objectName) {
            case "employees":
                mClassObject = Employee.class;
                break;
            case "flight":
                mClassObject = Flight.class;
                break;
            case "pairing":
                mClassObject = Pairing.class;
                break;
            case "sssignment":
                mClassObject = Assignment.class;
                break;
            default:
                fieldInfo.add("Неверное имя объекта");
                return new ArrayList[]{fieldInfo};
        }

        Field[] fields = mClassObject.getDeclaredFields();
        for ( Field value : fields) {
            fieldInfo.add( value.getName()+": " + value.getType());
            System.out.print(" --- getObjectInfo > "+value.getName()+": " + value.getType());
        }

        Object[] resultsArray = fieldInfo.toArray();

        return resultsArray;
    }


    @RequestMapping("getEmployeePairings")
    public Object[] getEmployeePairings(@RequestParam("Id") Long Id) {
        String sql =
                " SELECT a.employee_id, a.pairing_id,  a.rank,  p.date_time_start, p.date_time_end\n" +
                        " FROM teraplan.assignments a,  teraplan.pairings p\n" +
                        " WHERE a.pairing_id = p.id  AND a.employee_id = ?";

        List<Map<String, Object>> results = jdbcTemplate.queryForList(sql, new Object[]{Id} );



        Object[] resultsArray = results.toArray();

        return resultsArray;
    }
    @RequestMapping("getEmployeePairingFlights")
    public Object[] getEmployeePairingFlights(@RequestParam("Id") Long Id) {
        String sql =
                "SELECT a.employee_id, a.pairing_id,  a.rank,  p.date_time_start, p.date_time_end, \n" +
                "       f.date_time_arrival , f.date_time_departure ,f.flight_num, f.station_arrival, f.station_departure \n" +
                "FROM teraplan.assignments a,  teraplan.pairings p,  teraplan.legs l, teraplan.flights f\n" +
                "WHERE a.pairing_id = p.id AND p.id = l.pairing_id AND f.id = l.flight_id AND a.employee_id = ?";

        List<Map<String, Object>> results = jdbcTemplate.queryForList(sql, new Object[]{Id} );

        Object[] resultsArray = results.toArray();

        return resultsArray;
    }

    @GetMapping("pairingsDataUpload")
    public void pairingsDataUpload(){

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        File jsonFile;
        try {
            jsonFile = ResourceUtils.getFile("/var/tmp/roster.json");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

        RostersInfo rostersInfo;
        List<Pairing> pairingList;
        try {
            rostersInfo = objectMapper.readValue(jsonFile, RostersInfo.class);
            pairingList = List.of(rostersInfo.getPairings());
            pairingRepository.saveAll(pairingList);
        } catch (IOException e) {
            System.out.print(" --- IOException   ---");
            throw new RuntimeException(e);
        }
    }


    @RequestMapping("assignmentsDataUpload")
    public void assignmentsDataUpload(){

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        File jsonFile;
        try {
            jsonFile = ResourceUtils.getFile("/var/tmp/roster.json");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

        RostersInfo rostersInfo;
        Assignment[] assignments;
        try {
            rostersInfo = objectMapper.readValue(jsonFile, RostersInfo.class);
            assignments = rostersInfo.getAssignments();

            for (Assignment assignment : assignments) {
                if (assignment.getPairingID() > 0
                        && assignment.getEmployeeID() > 0
                        && (assignment.getRank() != null)
                ) {
                    try {
                        String idString = assignment.getEmployeeID() + ":" + assignment.getPairingID() + ":" + assignment.getRank();
                        Integer pairingID = Math.toIntExact(assignment.getPairingID());
                        Integer employeeID = Math.toIntExact(assignment.getEmployeeID());
                        String rank = assignment.getRank();
                        jdbcTemplate.update("INSERT INTO teraplan.assignments (id, rank, pairing_id, employee_id) VALUES (?, ?, ?, ?);",idString, rank, pairingID,employeeID );
                    } catch (Exception e) {
                        System.out.print("---Exception: ошибка загрузки назначения. Employee_id/Pairing_id/Rank="
                                + assignment.getEmployeeID() + "/" + assignment.getPairingID() + "/" + assignment.getRank());
                    }
                }
            }
        } catch (IOException e) {
            System.out.print(" --- IOException   ---");
            throw new RuntimeException(e);
        }
    }


    @RequestMapping("legsDataUpload")
    public void legsDataUpload(){

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        File jsonFile;
        try {
            jsonFile = ResourceUtils.getFile("/var/tmp/roster.json");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

        RostersInfoExt rostersInfoExt;
        PairingExt[] pairingExt;
        Dutie[] duties;
        Leg[] legs;

        String pairingId;
        try {
            rostersInfoExt =  objectMapper.readValue(jsonFile, RostersInfoExt.class);
            pairingExt = rostersInfoExt.getPairingExts();

            for (PairingExt ext : pairingExt) {
                pairingId = ext.getId();
                duties = ext.getDuties();
                if (duties != null) {
                    for (Dutie duty : duties) {
                        legs = duty.getLegs();
                        for (Leg leg : legs) {
                            if (leg.getFlightID() > 0 ) {
                                leg.setPairingID(Long.parseLong(pairingId));
                                try {
                                    String idString = leg.getFlightID() + ":" + leg.getPairingID();
                                    Integer flightID = Math.toIntExact(leg.getFlightID());
                                    Integer pairingID = Math.toIntExact(leg.getPairingID());
                                    jdbcTemplate.update("INSERT INTO teraplan.legs (id, flight_id, pairing_id) VALUES (?, ?, ?);",idString, flightID, pairingID );
                                } catch (Exception e) {
                                    System.out.print("---Exception: ошибка загрузки назначения рейса. getFlightID=" + leg.getFlightID());
                                }
                            }
                        }
                    }
                }

            }

        } catch (IOException e) {
            System.out.print(" --- IOException   ---");
            throw new RuntimeException(e);
        }
    }

    @RequestMapping("employeesDataUpload")
    public void employeesDataUpload(){

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        File jsonFile;
        try {
            jsonFile = ResourceUtils.getFile("/var/tmp/employees.json");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

        EmployeesInfo employeesInfo;
        List<Employee> employeeList;
        try {
            employeesInfo = objectMapper.readValue(jsonFile, EmployeesInfo.class);
            employeeList = List.of(employeesInfo.getEmployees());
            employeeRepository.saveAll(employeeList);
        } catch (IOException e) {
            System.out.print(" --- IOException   ---");
            throw new RuntimeException(e);
        }
    }

    @RequestMapping("flightsDataUpload")
    public void flightsDataUpload(){

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        File jsonFile;
        try {
            jsonFile = ResourceUtils.getFile("/var/tmp/flights.json");
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

        FlightsInfo flightsInfo;
        List<Flight> flightList;
        try {
            flightsInfo = objectMapper.readValue(jsonFile, FlightsInfo.class);
            flightList = List.of(flightsInfo.getFlights());
            flightRepository.saveAll(flightList);
        } catch (IOException e) {
            System.out.print(" --- IOException   ---");
            throw new RuntimeException(e);
        }
    }
}
