package org.teraplan.demo.repositories;

import org.springframework.data.repository.CrudRepository;
import org.teraplan.demo.entities.Assignment;

public interface AssignmentRepository extends CrudRepository <Assignment, String> {
}