package org.teraplan.demo.repositories;

import org.springframework.data.repository.CrudRepository;
import org.teraplan.demo.entities.VAssignment;

import java.util.Optional;

public interface VAssigmentRepository extends CrudRepository <VAssignment, Long> {
     Optional<VAssignment> findByEmployeeId(Long employee_id);

}
