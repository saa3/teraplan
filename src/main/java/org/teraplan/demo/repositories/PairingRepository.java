package org.teraplan.demo.repositories;

import org.springframework.data.repository.CrudRepository;
import org.teraplan.demo.entities.Pairing;

import java.util.Optional;

public interface PairingRepository extends CrudRepository <Pairing, Long> {

}
