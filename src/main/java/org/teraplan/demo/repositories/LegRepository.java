package org.teraplan.demo.repositories;

import org.springframework.data.repository.CrudRepository;
import org.teraplan.demo.entities.Leg;

public interface LegRepository extends CrudRepository <Leg, String> {
}
