package org.teraplan.demo.repositories;

import org.springframework.data.repository.CrudRepository;
import org.teraplan.demo.entities.Employee;

public interface EmployeeRepository extends CrudRepository <Employee, Long> {
}
