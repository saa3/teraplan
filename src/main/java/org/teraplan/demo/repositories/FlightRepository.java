package org.teraplan.demo.repositories;

import org.springframework.data.repository.CrudRepository;
import org.teraplan.demo.entities.Flight;

public interface FlightRepository extends CrudRepository < Flight, Long> {
}
