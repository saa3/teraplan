package org.teraplan.demo.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(schema = "teraplan", name = "legs")
@Getter
@Setter
public class Leg {

    @Id
    private String id;

    @JsonProperty("FlightID")
    private long FlightID;

    @JsonProperty("PairingID")
    private long PairingID;
}
