package org.teraplan.demo.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(schema = "teraplan", name = "v_assignmentrefs")
public class VAssignment {

    @Id
    private Long id;
    private Long employeeId;
    private Long pairing_id;
    private String fleet;
    private String gender;
    private String rank;
    private String date_time_start;
    private String date_time_end;
}
