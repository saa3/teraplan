package org.teraplan.demo.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(schema = "teraplan", name = "flights")
public class Flight {

    @Id
    @JsonProperty("ID")
    private long id;

    @JsonProperty("StationDeparture")
    private String StationDeparture;

    @JsonProperty("FlightNum")
    private String FlightNum;

    @JsonProperty("StationArrival")
    private String StationArrival;

    @JsonProperty("DateTimeDeparture")
    private String DateTimeDeparture;

    @JsonProperty("DateTimeArrival")
    private String DateTimeArrival;

}
