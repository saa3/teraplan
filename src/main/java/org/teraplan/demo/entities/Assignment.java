package org.teraplan.demo.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(schema = "teraplan", name = "assignments")
@Getter
@Setter
public class Assignment  {
    @Id
    private String id;

    @JsonProperty("EmployeeID")
    private long EmployeeID;
    @JsonProperty("PairingID")
    private long PairingID;
    @JsonProperty("Rank")
    private String Rank;

}
