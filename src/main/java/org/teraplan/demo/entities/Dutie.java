package org.teraplan.demo.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Dutie {

    @JsonProperty("DateTimeStart")
    private String DateTimeStart;
    @JsonProperty("DateTimeEnd")
    private String DateTimeEnd;

    @JsonProperty("Legs")
    private Leg[] Legs;
}
