package org.teraplan.demo.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;

@Entity
@Table(schema = "teraplan", name = "pairings")
@Getter
public class Pairing {

    @Id
    @JsonProperty("ID")
    private long id;

    @JsonProperty("Fleet")
    private String fleet;

    @JsonProperty("DateTimeStart")
    private String DateTimeStart;

    @JsonProperty("DateTimeEnd")
    private String DateTimeEnd;

    public Pairing() {
    }

    public Pairing(long id, String fleet, String dateTimeStart, String dateTimeEnd) {
    }
}
