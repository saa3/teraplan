package org.teraplan.demo.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class EmployeesInfo {

    @JsonProperty("Version")
    private String version;

    @JsonProperty("Employees")
    private Employee[] employees;
}
