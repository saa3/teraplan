package org.teraplan.demo.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class RostersInfoExt {

    @JsonProperty("Version")
    private String version;

    @JsonProperty("Pairings")
    private PairingExt[] pairingExts;
}
