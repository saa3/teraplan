package org.teraplan.demo.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class PairingExt {

    @JsonProperty("ID")
    private String id;

    @JsonProperty("Duties")
    private Dutie[] duties;
}
