package org.teraplan.demo.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(schema = "teraplan", name = "employees")
public class Employee {

    @Id
    @JsonProperty("ID")
    private long id;

    @JsonProperty("Fleet")
    private String Fleet;

    @JsonProperty("Gender")
    private String Gender;
}
