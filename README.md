# 22.03.2023
# Alexander Sergeychik
# v 1.0

# -- Общая информация

- Приведен вариант реализации взаимодействия помещенного в Docker-контейнер Spring-приложение с базой данных PostgresSQL
- Docker-файл Spring-приложения размещен в главном каталоге репозитория
- В качестве входных данных для Spring-приложения использованы json-файлы, которые должны бытьразмещены в каталоге локальной машины:
>/Users/docker/teraplan/app
- Примеры используемых json-файлов можно найти в каталоге проекта в папке
>src/main/resources

Для создания образа Spring-приложения использован скрипт
>/scripts/plugin/create-image-default.sh

- Скрипт создания и инициализации схемы данных PostgresSQL-контейнера размещен в каталоге проекта
>/scripts/postgres/initdb/init.sql


# - Запуск PostgresSQL

- Перейти в каталог проекта ./scripts/postgres
- Выполнить скрипт
>pgstart.sh

При этом будет стартован PostgresSQL-контейнер и запущен скрипт создания схемы данных

# - Запуск Приложения

Для запуска контейнера Spring-приложения использовать скрипт
>docker run   -p 4001:4001 --link postgres-conteiner --name teraplan -v /Users/docker/teraplan/app:/var/tmp/ asergeychik/teraplan-image



