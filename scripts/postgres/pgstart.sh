# Если сети network нет - создаем
result=$(docker network ls | grep mynetwork)
if [[ -n "$result" ]]; then
  echo 'mynetwork exists'
#  docker network rm mynetwork
else
  echo 'No mynetwork'
  docker network create mynetwork
fi

docker stop  postgres-conteiner
docker rm  postgres-conteiner
cd initdb

docker run -d -p 5432:5432 -d --net=mynetwork --name postgres-conteiner --hostname postgres-host  -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=postgres  -v "$(pwd)":/docker-entrypoint-initdb.d postgres:14.3


