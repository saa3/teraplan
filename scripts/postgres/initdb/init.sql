-- SCHEMA: teraplan
-- DROP SCHEMA IF EXISTS teraplan ;

CREATE SCHEMA IF NOT EXISTS teraplan
    AUTHORIZATION postgres;


-- Table: teraplan.employees
-- DROP TABLE IF EXISTS teraplan.employees;

CREATE TABLE IF NOT EXISTS teraplan.employees
(
    id bigint NOT NULL,
    fleet character varying(255) COLLATE pg_catalog."default",
    gender character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT employees_pkey PRIMARY KEY (id)
)
TABLESPACE pg_default;

ALTER TABLE IF EXISTS teraplan.employees
    OWNER to postgres;



-- Table: teraplan.flights
-- DROP TABLE IF EXISTS teraplan.flights;

CREATE TABLE IF NOT EXISTS teraplan.flights
(
    id bigint NOT NULL,
    flight_num character varying(255) COLLATE pg_catalog."default",
    date_time_arrival character varying(255) COLLATE pg_catalog."default",
    date_time_departure character varying(255) COLLATE pg_catalog."default",
    station_arrival character varying(255) COLLATE pg_catalog."default",
    station_departure character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT flights_pkey PRIMARY KEY (id)
)
TABLESPACE pg_default;

ALTER TABLE IF EXISTS teraplan.flights
    OWNER to postgres;



-- Table: teraplan.pairings
-- DROP TABLE IF EXISTS teraplan.pairings;

CREATE TABLE IF NOT EXISTS teraplan.pairings
(
    id bigint NOT NULL,
    date_time_end character varying(255) COLLATE pg_catalog."default",
    date_time_start character varying(255) COLLATE pg_catalog."default",
    fleet character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT pairings_pkey PRIMARY KEY (id)
)
TABLESPACE pg_default;

ALTER TABLE IF EXISTS teraplan.pairings
    OWNER to postgres;


-- Table: teraplan.assignmentrefs

-- DROP TABLE IF EXISTS teraplan.assignments;

CREATE TABLE IF NOT EXISTS teraplan.assignments
(
    id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    rank character varying(255) COLLATE pg_catalog."default" NOT NULL,
    pairing_id bigint NOT NULL,
    employee_id bigint NOT NULL,
    CONSTRAINT assignments_pkey PRIMARY KEY (id),
    CONSTRAINT "assignmentUK" UNIQUE (employee_id, pairing_id, rank),
    CONSTRAINT "employeeFK" FOREIGN KEY (employee_id)
        REFERENCES teraplan.employees (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT "pairingFK" FOREIGN KEY (pairing_id)
        REFERENCES teraplan.pairings (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)
TABLESPACE pg_default;

ALTER TABLE IF EXISTS teraplan.assignments
    OWNER to postgres;
-- Index: fki_employeeFK

-- DROP INDEX IF EXISTS teraplan."fki_employeeFK";

CREATE INDEX IF NOT EXISTS "fki_employeeFK"
    ON teraplan.assignments USING btree
    (employee_id ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: fki_pairingFK

-- DROP INDEX IF EXISTS teraplan."fki_pairingFK";

CREATE INDEX IF NOT EXISTS "fki_pairingFK"
    ON teraplan.assignments USING btree
    (pairing_id ASC NULLS LAST)
    TABLESPACE pg_default;

-- Table: teraplan.legs

-- DROP TABLE IF EXISTS teraplan.legs;


CREATE TABLE IF NOT EXISTS teraplan.legs
(

    flight_id bigint NOT NULL,
    pairing_id bigint NOT NULL,
    id character varying(255) COLLATE pg_catalog."default" NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT "legUK" UNIQUE (pairing_id, flight_id),
    CONSTRAINT "flightFK" FOREIGN KEY (flight_id)
        REFERENCES teraplan.flights (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT "pairingFK" FOREIGN KEY (pairing_id)
        REFERENCES teraplan.pairings (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)
TABLESPACE pg_default;

ALTER TABLE IF EXISTS teraplan.legs
    OWNER to postgres;
-- Index: fki_flightFK

-- DROP INDEX IF EXISTS teraplan."fki_flightFK";





-- View: teraplan.v_legs

-- DROP VIEW teraplan.v_legs;



CREATE OR REPLACE VIEW teraplan.v_legs
 AS
 SELECT pairings.fleet,
    pairings.date_time_start,
    pairings.date_time_end,
    legs.id,
    legs.flight_id,
    legs.pairing_id,
    flights.flight_num,
    flights.station_arrival,
    flights.station_departure
   FROM teraplan.legs,
    teraplan.flights,
    teraplan.pairings
  WHERE legs.flight_id = flights.id AND legs.pairing_id = pairings.id;

ALTER TABLE teraplan.v_legs
   OWNER TO postgres;



-- View: teraplan.v_assignments

-- DROP VIEW teraplan.v_assignments;


CREATE OR REPLACE VIEW teraplan.v_assignments
 AS
 SELECT e.id,
    a.employee_id,
    a.pairing_id,
    e.fleet,
    e.gender,
    a.rank,
    p.date_time_start,
    p.date_time_end
   FROM teraplan.assignments a,
    teraplan.employees e,
    teraplan.pairings p
  WHERE e.id = a.employee_id AND a.pairing_id = p.id AND e.fleet::text = p.fleet::text;



ALTER TABLE teraplan.v_assignments
    OWNER TO postgres;

