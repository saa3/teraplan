#!/bin/bash
IMAGE_NAME=asergeychik/teraplan-image

docker stop teraplan
docker rm teraplan

pushd ../../

mvn clean package

docker build -t   $IMAGE_NAME .

# get image info
docker image ls | grep $IMAGE_NAME